## Flexible Grid (Test)

### Initial setup

1. `git clone https://gitlab.com/du-hernandez/test-pragma.git`
2. `cd test-pragma`
2. `npm install` or `yarn install`

### Run the app

1. `npm start` or `yarn start`
2. In the browser, go to: `localhost:9000`